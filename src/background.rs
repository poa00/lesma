//
// background.rs
// Copyright (C) 2023 Óscar García Amor <ogarcia@connectical.com>
// Distributed under terms of the GNU GPLv3 license.
//

use std::path::PathBuf;

use lesma::{
    create_lesma_storage,
    create_sample_lesma,
    Lesma,
    lesma_has_expired,
    lesma_reached_max_views,
    read_meta_from_file
};

/// Background process that delete expired lesmas
pub async fn keep_house_clean(storage: &PathBuf) {
    let data_dir = storage.join("data");
    let meta_dir = storage.join("meta");
    loop {
        // Run every ten minutes
        rocket::tokio::time::sleep(std::time::Duration::from_secs(600)).await;
        trace!("Running house cleaning");
        if data_dir.is_dir() && meta_dir.is_dir() {
            // Read meta directory for lesmas
            match meta_dir.read_dir() {
                Ok(meta_dir_list) => {
                    for meta_file in meta_dir_list {
                        match meta_file {
                            Ok(meta_file) => {
                                // Read metadata
                                match read_meta_from_file(meta_file.path()) {
                                    Ok(lesma_meta) => {
                                        // Check if has expired or reached max views
                                        if lesma_has_expired(&lesma_meta) || lesma_reached_max_views(&lesma_meta) {
                                            // Get filename of metadata file (safe to unwrap since
                                            // it has readed before)
                                            let meta_file_file_name = meta_file.path().file_name().unwrap().to_str().unwrap().to_string();
                                            // Get a lesma manager
                                            let lesma = Lesma::from_id_hash(storage.to_path_buf(), "", &meta_file_file_name);
                                            // Delete expired lesma
                                            if let Err(e) = lesma.delete(&lesma_meta) {
                                                error!("I cannot keep the house clean, error deleting lesma {}, {}", meta_file.path().display(), e.to_string())
                                            }
                                        }
                                    },
                                    Err(e) => error!("I cannot keep the house clean, error reading meta file {}, {}", meta_file.path().display(), e.to_string())
                                }
                            },
                            Err(e) => error!("I cannot keep the house clean, meta error, {}", e.to_string())
                        }
                    }
                },
                Err(e) => error!("I cannot keep the house clean, cannot read meta dir, {}", e.to_string())
            }
        } else {
            // Create lesma storage directory structure
            if let Err(e) = create_lesma_storage(&storage) {
                println!("{}", e);
                std::process::exit(0x0100);
            }
        }
        // Create sample lesma
        if let Err(e) = create_sample_lesma(&storage) {
            println!("{}", e);
            std::process::exit(0x0100);
        }
        trace!("House cleaning completed");
    }
}
