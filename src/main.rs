//
// main.rs
// Copyright (C) 2023 Óscar García Amor <ogarcia@connectical.com>
// Distributed under terms of the GNU GPLv3 license.
//

#[macro_use] extern crate rocket;

use std::path::PathBuf;

use rocket::data::{ByteUnit, Limits, ToByteUnit};
use rocket::fairing::AdHoc;
use rocket::figment::{Figment, Profile, providers::{Env, Format, Serialized, Toml}};
use rocket::fs::FileServer;
use rocket::http::uri::Host;
use rocket::serde::{Deserialize, Serialize};
use rocket_dyn_templates::Template;

mod background;
mod models;
mod routes;

use crate::routes::{default_catcher, not_found, too_large};
use lesma::{create_lesma_storage, create_sample_lesma};

const APP_VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct LesmaConfig {
    pub https: bool,
    pub domains: Vec<Host<'static>>,
    pub storage: PathBuf,
    pub static_dir: PathBuf,
    pub text_size_limit: ByteUnit,
    pub binary_size_limit: ByteUnit,
    pub default_limit_plain: u8,
    pub default_limit_browser: u8,
    pub default_expire_plain: u32,
    pub default_expire_browser: u32,
    pub max_expire: u32
}

impl Default for LesmaConfig {
    fn default() -> LesmaConfig {
        LesmaConfig {
            https: false,
            domains: [Host::new(uri!("localhost:8000")), Host::new(uri!("127.0.0.1:8000"))].to_vec(),
            storage: [std::env::temp_dir(), "lesma".into()].iter().collect(),
            static_dir: if PathBuf::from("/usr/lib/lesma/static").is_dir() {
                PathBuf::from("/usr/lib/lesma/static")
            } else {
                PathBuf::from("static")
            },
            text_size_limit: 1.mebibytes(),
            binary_size_limit: 10.mebibytes(),
            default_limit_plain: 0, // No limit
            default_limit_browser: 0, // No limit
            default_expire_plain: 720, // 720 hours (30 days)
            default_expire_browser: 24, // 24 hour
            max_expire: 720 // 720 hours (30 days)
        }
    }
}

#[launch]
async fn rocket() -> _ {
    // Configure default template dir location
    let template_dir = if PathBuf::from("/usr/lib/lesma/templates").is_dir() {
        PathBuf::from("/usr/lib/lesma/templates")
    } else {
        PathBuf::from("templates")
    };

    // Get lesma configuration
    let figment = Figment::from(rocket::Config::default())
        .merge(("template_dir", template_dir))
        .merge(Serialized::defaults(LesmaConfig::default()))
        .merge(Toml::file("/etc/lesma.toml").nested())
        .merge(Toml::file("lesma.toml").nested())
        .merge(Env::prefixed("LESMA_").global())
        .select(Profile::from_env_or("LESMA_PROFILE", "release"));

    // Get storage static dirs, default expire and max expire (safe to unwrap since they have default value)
    let storage = figment.extract_inner::<PathBuf>("storage").unwrap();
    let static_dir = figment.extract_inner::<PathBuf>("static_dir").unwrap();
    let default_expire_plain = figment.extract_inner::<u32>("default_expire_plain").unwrap();
    let default_expire_browser = figment.extract_inner::<u32>("default_expire_browser").unwrap();
    let max_expire = figment.extract_inner::<u32>("max_expire").unwrap();

    // Perform expiration checks
    if default_expire_plain > max_expire {
        println!("Default expire plain ({}) cannot be longer then max expire ({})", default_expire_plain, max_expire);
        std::process::exit(0x0100);
    }
    if default_expire_browser > max_expire {
        println!("Default expire browser ({}) cannot be longer then max expire ({})", default_expire_browser, max_expire);
        std::process::exit(0x0100);
    }

    // Update limits (safe to unwrap since they have default value)
    let text_size_limit = figment.extract_inner::<ByteUnit>("text_size_limit").unwrap();
    let binary_size_limit = figment.extract_inner::<ByteUnit>("binary_size_limit").unwrap();
    let figment = figment.merge(("limits", Limits::new().limit("form", text_size_limit)))
        .merge(("limits", Limits::new().limit("data-form", binary_size_limit)))
        .merge(("limits", Limits::new().limit("file", binary_size_limit)));

    // Create lesma storage directory structure
    if let Err(e) = create_lesma_storage(&storage) {
        println!("{}", e);
        std::process::exit(0x0100);
    }

    // Create sample lesma
    if let Err(e) = create_sample_lesma(&storage) {
        println!("{}", e);
        std::process::exit(0x0100);
    }

    // Launch background cleaner
    rocket::tokio::spawn(async move {
        background::keep_house_clean(&storage).await;
    });

    // Launch!
    rocket::custom(figment)
        .attach(AdHoc::config::<LesmaConfig>())
        .attach(Template::fairing())
        .register("/", catchers![default_catcher, not_found, too_large])
        .mount("/", routes![
               routes::get_root,
               routes::post_root,
               routes::get_lesma,
               routes::post_lesma,
               routes::get_clone_lesma,
               routes::post_clone_lesma,
               routes::post_new_lesma,
               routes::get_help,
               routes::get_offline,
               routes::get_favicon,
               routes::get_lesma_js,
               routes::get_service_worker_js,
               routes::get_manifest
        ])
        .mount("/css", FileServer::from(static_dir.join("css")))
        .mount("/img", FileServer::from(static_dir.join("img")))
}
