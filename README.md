# lesma

(c) 2017-2023 Óscar García Amor

Redistribution, modifications and pull requests are welcomed under the terms
of GPLv3 license.

lesma is a simple paste app, friendly with browser and command line, with
files as storage backend. You can see in action in https://lesma.eu.

## Main features

* Written in [Rust][rust]
* One post → One download link
* To post data only curl is needed
* Set a view / download limit
* Set expiration time
* [Deduplication][deduplication], if two identical posts are sent only one
  is stored but different links are generated
* No database, the backend storage is files
* No dependencies or cron jobs to keep the house clean

[rust]: https://www.rust-lang.org/
[deduplication]: https://en.wikipedia.org/wiki/Data_deduplication

## Installation

### From binary

Simply download latest release from [releases page][releases]. You can use
[systemd unit][unit] from [Arch Linux package][package] to run it.
```shell
tar xf lesma-X.X.X-ARCH.tar.xz
cd lesma-X.X.X-ARCH
sudo mkdir -p /usr/lib/lesma
sudo cp -r static templates /usr/lib/lesma
sudo install -m755 lesma /usr/bin/lesma
sudo install -m644 lesma.toml.example /etc/lesma.toml
sudo vim /etc/lesma.toml # Configure lesma as you like
sudo curl 'https://aur.archlinux.org/cgit/aur.git/plain/lesma.service?h=lesma' \
  -o /etc/systemd/system/lesma.service
sudo systemctl start lesma
sudo systemctl enable lesma
```

Warning: The only binary provided is for `linux-amd64`, if you need to run
lesma on another architecture (like a Raspberry Pi) you must compile it, see
how to do it in the [from source section](#from-source).

Note that the systemd unit uses a dynamic user that has a persistent
directory in `/var/lib/lesma/` so it is recommended that the storage
directory point to this path.

[releases]: https://gitlab.com/ogarcia/lesma/-/releases
[unit]: https://aur.archlinux.org/cgit/aur.git/tree/lesma.service?h=lesma
[package]: https://aur.archlinux.org/packages/lesma

### With Docker

A docker image of lesma can be downloaded from [here][glcr] or from
[Docker Hub][hub].

To run it, simply exec.
```
docker run -t -d \
  --name=lesma \
  -p 8000:8000 \
  ogarcia/lesma
```

This start lesma and publish the port to host.

Warning: this is a basic run, all data will be destroyed after container
stop and rm.

[glcr]: https://gitlab.com/ogarcia/lesma/container_registry
[hub]: https://hub.docker.com/r/ogarcia/lesma

#### Persist data using a Docker volume

lesma Docker image uses a volume `/var/lib/lesma` to store pastes and files.
You can exec the following to mount it in your host as persistent storage.
```
docker run -t -d \
  --name=lesma \
  -p 8000:8000 \
  -v /my/lesma:/var/lib/lesma \
  ogarcia/lesma
```

Take note that you must create before the directory `/my/lesma` and set
ownership to UID/GID 100.
```
mkdir -p /my/lesma
chown -R 100:100 /my/lesma
```

#### Docker environment variables

| Variable | Used for | Default value |
| --- | --- | --- |
| `LESMA_ADDRESS` | Listen address | 0.0.0.0 |
| `LESMA_PORT` | Listen port | 8000 |
| `LESMA_HTTPS` | Generate the paste links with http or with https | false |
| `LESMA_DOMAINS` | Valid domains when generating URLs for new pastes | [localhost:8000, 127.0.0.1:8000] |
| `LESMA_TEXT_SIZE_LIMIT` | Size limit of uploaded text pastes or files (See below) | 1MiB |
| `LESMA_BINARY_SIZE_LIMIT` | Size limit of uploaded binary files (See below) | 10MiB |
| `LESMA_DEFAULT_LIMIT_PLAIN` | Default download limit for each paste or file created with a plain client like `curl` (0: unlimited; max: 255) | 0 |
| `LESMA_DEFAULT_LIMIT_BROWSER` | Default download limit for each paste or file created with a browser like Firefox (0: unlimited; max: 255) | 0 |
| `LESMA_DEFAULT_EXPIRE_PLAIN` | Default expiration time (in hours) for each paste or file created with a plain client | 720 (30 days) |
| `LESMA_DEFAULT_EXPIRE_BROWSER` | Default expiration time (in hours) for each paste or file created with a browser | 720 (30 days) |
| `LESMA_MAX_EXPIRE` | Max expiration time that an user can set (in hours) for each paste or file | 720 (30 days) |
| `LESMA_LOG_LEVEL` | Log level | normal |

For both text size limit and binary size limit you can simply specify
a number and the size will be in bytes, but you can use the suffixes kB, MB,
GB.. for powers of ten or KiB, MiB, GiB.. for powers of two.
The size limit sets both the limit for uploaded files and for the
information posted. If you simply indicate a number the size is in bytes but
you can use the suffixes kB, MB, GB.. for powers of ten or KiB, MiB, GiB..
for powers of two.

### From source

#### Installing Rust

lesma build has been tested with current Rust stable release version. You
can install Rust from your distribution package or use [`rustup`][rustup].
```
rustup default stable
```

If you prefer, you can use the stable version only for install lesma (you
must clone the repository first).
```
rustup override set stable
```

[rustup]: https://rustup.rs/

#### Installing lesma

To build lesma binary simply execute the following commands.
```sh
git clone https://gitlab.com/ogarcia/lesma.git
cd lesma
cargo build --release
```

After build the binary is located in `target/release/lesma`. The `templates`
and `static` directories are also needed, they can be with the executable or
in `/usr/lib/lesma`.

## Configuration

How lesma uses [Rocket][rocket] certain configuration parameters are
compatible with each other. You can look at the [Rocket configuration
documentation][rcdoc] to see what the basic parameters are. In any case,
a fully commented `lesma.toml.example` is provided and the most important
parameters are detailed below.

| Setting | Use | Default value |
| --- | --- | --- |
| `address` | Listen address | 127.0.0.1 |
| `port` | Listen port | 8000 |
| `https` | Generate the paste links with http or with https | false |
| `domains` | Valid domains when generating URLs for new pastes | ["localhost:8000", "127.0.0.1:8000"] |
| `text_size_limit` | Size limit of uploaded text pastes | 1MiB |
| `binary_size_limit` | Size limit of uploaded files | 10MiB |
| `default_limit_plain` | Default download limit for each paste or file created with a plain client like `curl` (0: unlimited; max: 255) | 0 |
| `default_limit_browser` | Default download limit for each paste or file created with a browser like Firefox (0: unlimited; max: 255) | 0 |
| `default_expire_plain` | Default expiration time (in hours) for each paste or file created with a plain client | 720 (30 days) |
| `default_expire_browser` | Default expiration time (in hours) for each paste or file created with a browser | 720 (30 days) |
| `max_expire` | Max expiration time that an user can set (in hours) for each paste or file | 720 (30 days) |

If you don't want use a config file you can use environment variables. For
example to generate links with https and listen in 8080.
```
export LESMA_PORT=8080
export LESMA_HTTPS=true
export LESMA_DOMAINS="[internal:8080, external.example.com]"
export LESMA_STORAGE=/tmp/lesma
lesma
```

[rocket]: https://rocket.rs
[rcdoc]: https://rocket.rs/v0.5/guide/configuration/#configuration
